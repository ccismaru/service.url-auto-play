import time
import xbmc
import xbmcaddon
import xbmcgui

def log(str, level=xbmc.LOGNOTICE):
    
    str = xbmcaddon.Addon().getAddonInfo("name") + ": " + str
    
    xbmc.log(str, level=level)

if __name__ == '__main__':
    monitor = xbmc.Monitor()

    onHome = True
    isPlaying = False

    maxTime = 5
    currTime = maxTime

    while not monitor.abortRequested():
        if monitor.waitForAbort(1):
            # Abort was requested while waiting. We should exit
            break

        if not int(time.time()) % 30:
            log("is running @ %d" % int(time.time()), level=xbmc.LOGNOTICE)

        isPlaying = xbmc.getCondVisibility("Player.Playing")

        if isPlaying:
            continue

        winId = xbmcgui.getCurrentWindowId()
        dialogId = xbmcgui.getCurrentWindowDialogId()

        if winId != 10000 or dialogId > 10000:
            currTime = maxTime
            continue

        if winId == 10000:
            if currTime > 0:
                xbmc.executebuiltin("Notification(Auto Play URL, Starts in " + str(currTime) + " sec, 1000)", False)
                currTime = currTime - 1
                continue

            if not isPlaying:
                url = xbmcaddon.Addon().getSetting("url")
                xbmc.Player().play(url)
                currTime = maxTime
